﻿(function () {
	var app = angular.module("SISRetrospectives");
	var FeedbackService = function ($http) {
           var readAll = function () {
               return $http.get("../API/Feedback/?" + "_" + $.now()).then(function (response) {
                return response.data;

            })

            }
           var addFeedback = function (data) {
	        return $http.post("../API/Feedback/", data).then(function (response) {
	            return response.data;

	        })

	    }
	    return {
	        readAll: readAll,
	        addFeedback: addFeedback
	        }

	}
	app.factory("FeedbackService", FeedbackService)
})();
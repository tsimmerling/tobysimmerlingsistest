﻿(function () {
    var app = angular.module("SISRetrospectives");

    var RetrospectiveService = function ($http) {
        var readAll = function () {
            return $http.get("../API/Retrospective/?"+ "_"+ $.now() ).then(function (response) {
                return response.data;

            })

        }

        var addRetrospective = function (data) {
            return $http.post("../API/Retrospective/", data).then(function (response) {
                return response.data;

            })
        }
        return {
            readAll: readAll,
            addRetrospective: addRetrospective
        };

    }

    app.factory("RetrospectiveService", RetrospectiveService);
})();
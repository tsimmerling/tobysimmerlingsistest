﻿(function () {
	var app = angular.module("SISRetrospectives");


	var SISRetrospectiveController = function ($scope, RetrospectiveService, FeedbackService) {
		var onRetro = function (data) {
			$scope.retros = data;
		}
		var onError = function (Reason) {
			$scope.error = Reason;

		}
		var onRetroAdd = function () {
			RetrospectiveService.readAll().then(onRetro, onError);
		}
		var onFeedbackAdd = function () {

		}
		$scope.addRetro = function () {
		    RetrospectiveService.addRetrospective(this.newRetrospective).then(onRetroAdd, onError);
        }
		$scope.addFeedback = function () {
		    FeedbackService.addFeedback(this.newFeedback);
		}
		
		$scope.addParticpant = function () {
		    $scope.newRetrospective.Participants.push('');
		}
		$scope.showFeedback = function (retroId) {
		    $scope.newFeedback = {};
		    $scope.newFeedback.retrospectiveId = retroId;
		}
		$scope.showRetrospective = function () {
		    $scope.newFeedback.retrospectiveId = null;

		}
        RetrospectiveService.readAll().then(onRetro, onError)
        $scope.newRetrospective = { Participants: [] };
        
	}

    
	app.controller("SISRetrospectiveController", SISRetrospectiveController)

})();
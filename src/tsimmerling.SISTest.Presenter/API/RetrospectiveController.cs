﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tsimmerling.SISTest.Service.Services.Interfaces;
using tsimmerling.SISTest.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace tsimmerling.SISTest.Presenter.API
{
    [Route("api/[controller]")]
    public class RetrospectiveController : Controller
    {
        public IRetrospectiveService retrospectiveService { get; private set; }

        public RetrospectiveController(IRetrospectiveService RetrospectiveService)
        {
            retrospectiveService = RetrospectiveService;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var response = retrospectiveService.ReadAll();
                return Ok(response);
            }
            catch (Exception)
            {

                return BadRequest("Error Searching for Retrospectives");
            }
        }

        // GET api/values/5
        [HttpGet("{RetrospectiveDate}")]
        public IActionResult Get(DateTime RetrospectiveDate)
        {
            try
            {
                var response = retrospectiveService.Search(RetrospectiveDate);
                return Ok(response);
            }
            catch (Exception)
            {

                return BadRequest("Error Searching for Retrospectives");
            }


        }


        // POST api/values
        [HttpPost("")]
        public IActionResult Post([FromBody]Retrospective Model)
        {
            if (ModelState.IsValid)
            {
                int response = retrospectiveService.AddRetrospective(Model);
                if (response > 0)
                {
                    return Ok(response);

                }
                else
                {
                    return BadRequest("Error Adding Retrospective");

                }

            }
            else
            {
                return BadRequest("Invalid Data");
            }

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return BadRequest("You can't delete Retrospectives");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tsimmerling.SISTest.Models;
using tsimmerling.SISTest.Service.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace tsimmerling.SISTest.Presenter.API
{
    [Route("api/[controller]")]
    public class FeedbackController : Controller
    {
        public IFeedbackService feedbackService { get; private set; }

        public FeedbackController(IFeedbackService FeedbackService)
        {
            feedbackService = FeedbackService;
        }
        // GET api/values/5
        [HttpGet("{RetrospectiveId}")]
        public List<Feedback> Get(int RetrospectiveId)
        {
            //todo error handling returning bad request or OK
            return feedbackService.Search(RetrospectiveId);
        
        }
        

        [HttpPost]
        public void Post(Feedback model)
        {
            feedbackService.addFeedback(model);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return BadRequest("You Can't Delete Feedback, sorry.");
        }
    }
}

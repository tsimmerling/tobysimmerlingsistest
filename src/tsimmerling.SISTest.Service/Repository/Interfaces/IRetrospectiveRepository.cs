﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.Models;
namespace tsimmerling.SISTest.Service.Repository.Interfaces
{
    public interface IRetrospectiveRepository
    {
        Retrospective Add(Retrospective Model);
        List<Retrospective> Search(DateTime? RetrospectiveDate = null);
    }
}

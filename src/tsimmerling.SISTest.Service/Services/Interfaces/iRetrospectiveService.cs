﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.Models;

namespace tsimmerling.SISTest.Service.Services.Interfaces
{
    public interface IRetrospectiveService
    {
        int AddRetrospective(Retrospective Model);
        List<Retrospective> ReadAll();
        List<Retrospective> Search(DateTime RetrospectiveDate);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.Models;

namespace tsimmerling.SISTest.Service.Services.Interfaces
{
    public interface IFeedbackService
    {
        int addFeedback(Feedback model);
        List<Feedback> Search(int RetrospectiveID);

    }
}

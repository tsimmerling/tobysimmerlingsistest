﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.Models;
using tsimmerling.SISTest.Service.Repository.Interfaces;
using tsimmerling.SISTest.Service.Services.Interfaces;

namespace tsimmerling.SISTest.Service.Services.Implementations
{
    public class FeedbackService : IFeedbackService
    {
        private IFeedbackRepository feedbackRepository { get; set; }


        public FeedbackService(IFeedbackRepository FeedbackRepository)
        {
            feedbackRepository = FeedbackRepository;
        }
        public int addFeedback(Feedback Model)
        {
            var response = feedbackRepository.Add(Model);
            return response.ID;
        }

        public List<Feedback> Search(int RetrospectiveID)
        {
            return feedbackRepository.Search(RetrospectiveID);
        }
    }
}

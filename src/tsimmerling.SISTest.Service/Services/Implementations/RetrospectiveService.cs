﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.Models;
using tsimmerling.SISTest.Service.Repository.Interfaces;
using tsimmerling.SISTest.Service.Services.Interfaces;

namespace tsimmerling.SISTest.Service.Services.Implementations
{
    public class RetrospectiveService : IRetrospectiveService
    {
        private IRetrospectiveRepository retrospectiveRepository { get; set; }

        public RetrospectiveService(IRetrospectiveRepository RetrospectiveRepository )
        {
            retrospectiveRepository = RetrospectiveRepository;
        }
        public int AddRetrospective(Retrospective Model)
        {
            var response =  retrospectiveRepository.Add(Model);
            return response.ID;
        }

        public List<Retrospective> ReadAll()
        {
            return retrospectiveRepository.Search();
        }

        public List<Retrospective> Search(DateTime RetrospectiveDate)
        {
            return retrospectiveRepository.Search(RetrospectiveDate);
        }
    }
}

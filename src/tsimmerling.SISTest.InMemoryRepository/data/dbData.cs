﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.Models;

namespace tsimmerling.SISTest.InMemoryRepository.data
{
    public static class dbData
    {

        public static List<Retrospective> Retrospectives { get; set; }
        public static List<Feedback> Feedbacks { get; set; }
    }
}

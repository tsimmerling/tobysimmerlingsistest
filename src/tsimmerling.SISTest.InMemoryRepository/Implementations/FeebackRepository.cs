﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.InMemoryRepository.data;
using tsimmerling.SISTest.Models;
using tsimmerling.SISTest.Service.Repository.Interfaces;

namespace tsimmerling.SISTest.InMemoryRepository.Implementations
{
    public class FeedbackRepository : IFeedbackRepository
    {
        public FeedbackRepository()
        {
            if(dbData.Feedbacks == null)
            {
                dbData.Feedbacks = new List<Feedback>();

            }
        }
        public Feedback Add(Feedback Model)
        {
            int ID;
            if (dbData.Feedbacks.Any())
            {
                ID = dbData.Feedbacks.OrderBy(x => x.ID).Last().ID + 1;
            }
            else
            {
                ID = 1;
            }

            Model.ID = ID;
            dbData.Feedbacks.Add(Model);
            return dbData.Feedbacks.Last();
        }

        public List<Feedback> Search(int RetrospectiveID)
        {
            return dbData.Feedbacks.Where(x => x.RetrospectiveID == RetrospectiveID).ToList();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tsimmerling.SISTest.InMemoryRepository.data;
using tsimmerling.SISTest.Models;
using tsimmerling.SISTest.Service.Repository.Interfaces;

namespace tsimmerling.SISTest.InMemoryRepository.Implementations
{
    public class RetrospectiveRepository : IRetrospectiveRepository
    {
        public RetrospectiveRepository()
        {
            if(dbData.Retrospectives == null)
            {

                dbData.Retrospectives = new List<Retrospective>();
                dbData.Retrospectives.Add(new Retrospective { Name = "Sample Retrospective", ID = 1, Date = System.DateTime.Now });
            }
        }
        public Retrospective Add(Retrospective Model)
        {
            int ID;
            if (dbData.Retrospectives.Any())
            {
                ID = dbData.Retrospectives.OrderBy(x => x.ID).Last().ID + 1;
            }
            else
            {
                ID = 1;
            }

            Model.ID = ID;
            dbData.Retrospectives.Add(Model);

            return dbData.Retrospectives.Last();
        }

        public List<Retrospective> Search(DateTime? RetrospectiveDate = default(DateTime?))
        {
            if (RetrospectiveDate != null)
            {

                return dbData.Retrospectives.Where(x => x.Date.Date == RetrospectiveDate.Value.Date).ToList();
            }
            else
            {
                return dbData.Retrospectives;

            }
        }
    }
}

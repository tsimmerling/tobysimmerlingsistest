﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsimmerling.SISTest.Models
{
    public class Feedback
    {
        public int ID { get; set; }
		//todo make a person model so that particpants and feedback people are from the same source
		public String FeedbackProvider { get; set; }
        public string Body { get; set; }
        public string FeedbackType { get; set; }
        public int RetrospectiveID { get; set; }
    }
	public enum FeedbackTypes
    {
        positive, negative, idea, praise

    }
 }
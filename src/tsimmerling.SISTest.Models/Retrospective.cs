﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsimmerling.SISTest.Models
{
    public class Retrospective
    {
        public int ID { get; set; }
        [Required]
        public String Name { get; set; }
        public string Summary { get; set; }
        [Required]
        public DateTime Date { get; set; }
        //todo make a person model so that particpants and feedback people are from the same source
        [Required]
        public string[] Participants {get;set;}
        
    }
}

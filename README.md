Here is the test as requested, sorry but I misjudged the time. I spend too much time on the API's and not enough on the tests and GUI.

I've implemented IOC on the service and repository, hoped to switch out the in memory repository for a entity framework one but there wasn't time

You can add and remove retrospectives, and there participants, but the feedback associated with them are not working. GUI has functionality but doesn't go anywhere.

The solution build via Team City and was published to azure by visual studio(MSBuild).

End points for the API are in {route}/API/Feedback and {route}/API/retrospectives, but as I said the feedback one is not complete.

There is a working app here if you want a play
http://tsimmerlingsistestpresenter20161113075423.azurewebsites.net/index.html

I did one very simple test just to show I could, but clearly this is quite far from TDD